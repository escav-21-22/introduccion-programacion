using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PruebaTransform : MonoBehaviour
{
    public float escala;

    void Start()
    {

        // Imprime por consola los valores X, Y y Z de la posici�n del objeto
        Debug.Log("xpos: " + transform.position.x);
        Debug.Log("ypos: " + transform.position.y);
        Debug.Log("zpos: " + transform.position.z);

        // Modifica la posici�n del objeto, situ�ndolo en la posici�n (1,2,3)
        transform.position = new Vector3(1, 2, 3);

        // Modifica la posici�n del objeto, modificando �nicamente su posici�n en el eje X
        transform.position = new Vector3(7, transform.position.y, transform.position.z);

        // Modifica la rotaci�n del objeto, girando 90� cada eje
        transform.rotation = Quaternion.Euler(90, 90, 90);

        // Modifica la escala del objeto, proporcionalmente seg�n la variable escala
        transform.localScale = escala * transform.localScale;

    }
}